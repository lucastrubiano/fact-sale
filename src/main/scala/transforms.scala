package org.novakorp.com

import org.apache.spark.sql.{DataFrame, Row, SaveMode}
import org.apache.spark.sql.functions.col
import org.apache.spark.util.LongAccumulator

object transforms extends SparkSessionWrapper {

  def readDataFrame(query:String): DataFrame = {
    val newDf = spark.sql(query)
    newDf
  }

  def getHashes(notCierreDf: DataFrame, cierreDf: DataFrame, insertFlag: Boolean): (DataFrame,DataFrame) = {
    import spark.implicits._
    /**
     * notCierreDf tiene todos los hashes de los otros cierres que ya fueron insertados.
     * cierreDf tiene los hashes que quiero que queden visibles.
     * Tengo que dividir al cierreDf en hashes que se deben insertar y en hashes que se deben levantar como particion.
     */


    val hashesToAddDf = if (insertFlag) {
      cierreDf.as('df1)
        .join( notCierreDf.as('df2) )
        .where($"df1.hashvalue" === $"df2.hashvalue")
        .select($"df1.*")
    } else {
      cierreDf
    }

    val hashesToInsertDf = if (insertFlag) {
      cierreDf.as("df1")
        .join(notCierreDf.as("df2"),
          $"df1.hashvalue" === $"df2.hashvalue",
          "leftanti")
    } else {
      spark.emptyDataFrame
    }

    (hashesToAddDf,hashesToInsertDf)
  }

  def insertFactSale(df: DataFrame, database: String, outputTable: String): Unit = {

    val columns = spark.sql(s"show columns in $database.$outputTable").collect().map(_.getString(0).trim)
    val dfToInsert = df.select(columns.map(col):_*)

    dfToInsert.write
      .mode("append")
      .insertInto(s"$database.$outputTable")

    println(s"Tabla $outputTable cargada")
  }

  def addPartition(row: Row, yearMonth:String, accum: LongAccumulator): Unit = {
    println(s"alter table br_cur.w_sale_f add partition (year_month='$yearMonth', hashvalue='${row(1)}')")
    accum.add(1)
    // HAY QUE PARAMETRIZAR LA BASE DE DATOS -> ${country}_ref.w_sale_f
    spark.sql(s"alter table br_cur.w_sale_f add partition (year_month='$yearMonth', hashvalue='${row(1)}')")
  }
}
