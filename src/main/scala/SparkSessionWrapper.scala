package org.novakorp.com

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf

trait SparkSessionWrapper extends Serializable {

  lazy val conf: SparkConf = new SparkConf()
    .setAppName("FactSale")
    .setMaster("yarn")
    .set("spark.executor.memory", "12g")
    .set("spark.driver.memory", "12g")
    .set("spark.executor.instances", "4")
    .set("hive.exec.dynamic.partition", "true")
    .set("hive.exec.dynamic.partition.mode", "nonstrict")
    .set("hive.exec.max.dynamic.partitions", "2000")
    .set("spark.sql.sources.partitionOverwriteMode","dynamic")
    .set("spark.sql.hive.convertMetastoreParquet","false")
    .set("spark.hadoop.metastore.catalog.default","hive")


  lazy val spark: SparkSession = SparkSession.builder().config(conf).enableHiveSupport().getOrCreate()
}
