package org.novakorp.com
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions.{col, lit, max}
import org.apache.spark.util.LongAccumulator

object entry extends SparkSessionWrapper {

  def main(args: Array[String]): Unit = {

    val environment: String       = args(0)
    val country: String           = args(1)
    val outputTable:String        = args(2)
    val processDate: String       = args(3)
    val idCierre: String          = args(4)
    val yearMonth: String         = args(5)
    val queryHisDetalles: String  = args(6)
    val queryProvImporLog: String = args(7)
    val forceOverwrite: Boolean   = args(8).toBoolean

    val database = s"${environment}_${country}_cur"
    val outputFilePath = s"hdfs://arbd1cl-ns/user/admin/$country/02-cur/$outputTable"

    spark.sparkContext.setLogLevel("ERROR")

    try {
      if (!forceOverwrite) {
        // READ DATAFRAME
        // """select  * from br_cur.provider_import_log where year_month  = @year_month"""
        val hashQuery: String = queryProvImporLog.replace("@year_month",yearMonth)
        val allHashesDf: DataFrame = transforms.readDataFrame(hashQuery)
        allHashesDf.cache()

        // CHECK IF IDCIERRE IS THE LAST IDCIERRE
        val lastLoadedDate: DataFrame = allHashesDf.select(max("process_date"))
        val currentDate: DataFrame = allHashesDf.filter(s"id_cierre == $idCierre").select(max("process_date"))

        println(lastLoadedDate.first().mkString)
        println(currentDate.first().mkString)

        // TRUE => INSERT / ADD ---- FALSE => ONLY ADD
        val insertFlag: Boolean = if (lastLoadedDate.first().mkString == currentDate.first().mkString){true} else{false}
        val accum: LongAccumulator = spark.sparkContext.longAccumulator("SumAccumulator")

        // GET DATAFRAME OF HASHES TO INSERT AND HASHES TO ADD PARTITION
        val cierreDf = allHashesDf.filter(s"id_cierre == $idCierre")
        val notCierreDf = allHashesDf.filter(s"id_cierre != $idCierre")

        val (hashToAddDf,hashToInsertDf) = transforms.getHashes(notCierreDf, cierreDf, insertFlag)


        // ADD OLD PARTITIONS
        val hashToAddList: Array[Row] = hashToAddDf.select("provider", "hashvalue").collect()
        hashToAddList.foreach(row => transforms.addPartition(row, yearMonth, accum))
        println(s"Se agregaron ${accum.value.toString} particiones.")


        // INSERT TO W_SALE_F IF NECESSARY
        if (insertFlag) {
          val providerList: Array[String] = hashToInsertDf.select("provider")
            //.where(s"id_cierre == ${idCierre}")
            .collect()
            .map(row => row.toString.replace("[","'").replace("]","'"))

          println(s"Hay ${providerList.length} providers a insertar")

          val queryToInsert = queryHisDetalles.replace("@year_month",yearMonth).replace("@provider_list",providerList.mkString(","))
          println(s"La query para insertar a his_detalles es ${queryToInsert}")

          val hisDetallesDf: DataFrame = transforms.readDataFrame(queryToInsert)
          val factSaleDf = hisDetallesDf.withColumn("year_month",lit(yearMonth))
            .withColumn("process_date", lit(processDate))

          transforms.insertFactSale(factSaleDf, database, outputTable)
        }
      } else {
        val queryToInsert = queryHisDetalles.replace("@year_month","202202").split("and provider in").head

        val hisDetallesDf: DataFrame = transforms.readDataFrame(queryToInsert)
        val factSaleDf = hisDetallesDf.withColumn("year_month",lit(yearMonth))
          .withColumn("process_date", lit(processDate))

        transforms.insertFactSale(factSaleDf, database, outputTable)
      }


    }
    catch {
      case e: Exception =>
        throw new RuntimeException(e.getStackTrace.mkString("\n"))
    }
    finally{
      spark.sparkContext.stop()
    }

  }

}
